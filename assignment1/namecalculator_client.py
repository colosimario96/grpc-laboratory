from __future__ import print_function

import grpc

import namecalculator_pb2
import namecalculator_pb2_grpc


def run():
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    with grpc.insecure_channel('grpc_server:50051') as channel:
        stub = namecalculator_pb2_grpc.nameCalculatorStub(channel)
        response = stub.CalculateCost(namecalculator_pb2.HelloRequest(name='Mario', cost=3))
    print("Greeter client received: " + response.message)


if __name__ == '__main__':
    run()
