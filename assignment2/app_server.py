from concurrent import futures
import time
import grpc
import definitions_pb2
import definitions_pb2_grpc

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

def sum(a,b):
    return a+b
def sub(a,b):
    return a-b
def mux(a,b):
    return a*b
def div(a,b):
    return a/b

operation = {"+": sum, "-": sub, "*": mux, "/": div}

class Application(definitions_pb2_grpc.ApplicationServicer):
    def calculate(self, request, context):
        print("Calculating {} {} {}".format(request.number1, request.operation, request.number2))
        try:
            result = operation[request.operation](float(request.number1), float(request.number2))
            msg = "{} {} {} = {}".format(request.number1, request.operation, request.number2, result)
        except:
            msg = "Invalid operation"
        return definitions_pb2.MessageText(text=msg)


    def elaborateString(self, request, context):
        result = ""
        for ch in request.text:
            result += str(ord(ch.upper())-64)+"0"
        msg = "Numerical representation of string '{}' is: {}".format(request.text, result)
        return definitions_pb2.MessageText(text=msg)

    def nameStream(self, requests, context):
        N_words = 0
        N_letters = 0
        for r in requests:
            N_words += 1
            N_letters += len(r.text)
        msg = "Number of words: {}\nNumber of letters: {} ".format(N_words, N_letters)
        return definitions_pb2.MessageText(text=msg)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    definitions_pb2_grpc.add_ApplicationServicer_to_server(Application(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    print("Server started")
    serve()
