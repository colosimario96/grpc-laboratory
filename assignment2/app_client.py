import grpc
import definitions_pb2_grpc
import definitions_pb2
from time import sleep

def calculate(op, number1, number2):
    with grpc.insecure_channel('grpc_server:50051') as channel:
        stub = definitions_pb2_grpc.ApplicationStub(channel)
        response = stub.calculate(definitions_pb2.calcInputs(operation=op, number1=number1, number2=number2))
    print(response.text)


def elaborateString(my_string):
    with grpc.insecure_channel('grpc_server:50051') as channel:
        stub = definitions_pb2_grpc.ApplicationStub(channel)
        response = stub.elaborateString(definitions_pb2.MessageText(text=my_string))
    print(response.text)


def words_iterator(words):
    for w in words:
        yield definitions_pb2.MessageText(text=w)

def nameStream(words):
    with grpc.insecure_channel('grpc_server:50051') as channel:
        stub = definitions_pb2_grpc.ApplicationStub(channel)
        current_word = words_iterator(words)
        response = stub.nameStream(current_word)
        print(response.text)


if __name__ == '__main__':
    print("Calculations:")
    calculate("+", 15, 52)
    calculate("-", 10, 8)
    calculate("*", 2, 7)
    calculate("/", 27, 3)
    print("\nString elaboration:")
    elaborateString("distributedsystems")

    names = ["Mario", "Susanna", "Miriam", "Fabio"]
    print("\nStream of names:", names)
    nameStream(names)
